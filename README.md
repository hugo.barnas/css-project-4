# css-project-4

page call to action in responsive

## Techno used

- Visual Studio code

- flex

## Results

1. Mobile

![Capture_d_écran_2021-12-05_à_18.17.37](/uploads/1873dc8c4dc9bb420199bad8d8c78ce1/Capture_d_écran_2021-12-05_à_18.17.37.png)


2. Desktop

![Capture_d_écran_2021-12-05_à_18.22.22](/uploads/eba8cf947437e343bdc8232f400c2551/Capture_d_écran_2021-12-05_à_18.22.22.png)



## Authors and acknowledgment
[10 projects in CSS in 10 days](https://dev.to/coderamrin/build-10-css-projects-in-10-days-project-4-27n7)

## License
For open source projects, say how it is licensed.


